// Variables

var score = 0;
var valeur = 1;

//Traitement des données
function Datas() {
    this.a = {
        valeur: 1,
        nombre: 0,
        cout: 10
    };
    this.b = {
        valeur: 3,
        nombre: 0,
        cout: 100
    };
    this.c = {
        valeur: 10,
        nombre: 0,
        cout: 500
    };
}
var donnees = new Datas();
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ["A", "B", "C"],
        datasets: [{
            label: 'Part des upgrades',
            data: [donnees.a.nombre, donnees.b.nombre, donnees.c.nombre],
            backgroundColor: [
                'rgba(255, 132, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)'
            ],
            borderColor: [
                'rgba(255,132,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        title: {
            display: true,
            text: 'Part des upgrades',
            fontColor: 'black'
        },
        legend: {
            labels: {
                fontColor: 'black'
            }
        }
    }
});

// Fonctions

function ajouter() {
    score += valeur;
    score = parseFloat(score.toFixed(2));
    updateData();
}

function acheter(id) {
    if (donnees[id].cout <= score) {
        donnees[id].nombre++;
        score -= Math.round(donnees[id].cout);
        score = parseFloat(score.toFixed(2));
        donnees[id].cout += Math.round(donnees[id].cout * 0.2);
        updateGraph();
        updateData();
    } else {
        myFunction();
    }
}

function ajoutSeq() {
    var ratio = 10;
    score += ((donnees.a.nombre * donnees.a.valeur) + (donnees.b.nombre * donnees.b.valeur) + (donnees.c.nombre * donnees.c.valeur)) / ratio;
    score = parseFloat(score.toFixed(2));
    updateData();
    setTimeout('ajoutSeq()', 1000 / ratio);
}
ajoutSeq();

function updateData() {
    document.getElementById("scorePerSec").innerHTML = (donnees.a.nombre * donnees.a.valeur) + (donnees.b.nombre * donnees.b.valeur) + (donnees.c.nombre * donnees.c.valeur) + "/sec";
    for (i of "abc") {
        document.getElementById('cout_' + i).innerHTML = "Coût : " + donnees[i].cout;
        document.getElementById('nb_' + i).innerHTML = i.toUpperCase() + " : " + donnees[i].nombre;
    }
    move();
    document.getElementById('score').innerHTML = score;
}

function updateGraph() {
    myChart.data.datasets[0].data = [donnees.a.nombre, donnees.b.nombre, donnees.c.nombre];
    myChart.update();
}

function move() {
    var abc = ["a", "b", "c"];
    var elem = new Array(abc.length);
    var width = new Array(abc.length);
    for (i in abc) {
        elem[i] = document.getElementById("myBar_" + abc[i]);
        width[i] = Math.round((score / donnees[abc[i]].cout) * 100);
        if (width[i] > 100) {
            width[i] = 100;
        }
        elem[i].style.width = width[i] + "%";
    }
}